// Copyright (c) 2025 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <emilua_qt/module.hpp>

#include <thread>

#include <QtGui/QGuiApplication>

namespace emilua_qt {

void init_loop(QGuiApplication& a);
std::error_code init_lua_module(lua_State* L);

std::error_code module_::init_lua_module(
    std::shared_lock<std::shared_mutex>&, emilua::vm_context& /*vm_ctx*/,
    lua_State* L)
{
    return ::emilua_qt::init_lua_module(L);
}

} // namespace emilua_qt

namespace emilua::main {

static int argc = 1;
static const char* argv[] = { "emilua" };

BOOST_SYMBOL_EXPORT void run(app_context& appctx, asio::io_context& ioctx)
{
    QGuiApplication app{argc, (char**)argv};
    app.setQuitOnLastWindowClosed(false);
    emilua_qt::init_loop(app);

    std::thread t{[&]() {
        ioctx.run();

        std::unique_lock<std::mutex> lk{appctx.extra_threads_count_mtx};
        while (appctx.extra_threads_count > 0)
            appctx.extra_threads_count_empty_cond.wait(lk);

        QMetaObject::invokeMethod(&app, "exit", Qt::QueuedConnection);
    }};

    std::ignore = app.exec();
    t.join();
}

} // namespace emilua::main
