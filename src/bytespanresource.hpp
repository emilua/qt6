// Copyright (c) 2023 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <emilua_qt/bytespanresource.hpp>

namespace emilua_qt {

ByteSpanResource::ByteSpanResource(QObject* parent)
    : QObject{parent}
{}

} // namespace emilua_qt
