// Copyright (c) 2024 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <emilua_qt/standarditemmodel.hpp>
#include <emilua_qt/image.hpp>
#include <QtCore/QByteArray>
#include <QtCore/QCoreApplication>
#include <algorithm>
#include <boost/hana/functional/overload.hpp>
#include <emilua/core.hpp>

namespace emilua_qt {

using emilua::tostringview;

namespace hana = boost::hana;

StandardItemModel::StandardItemModel(int rows, int columns)
    : rowCount{rows}
    , columnCount{columns}
{}

StandardItemModel::GUIData::GUIData(int rows, int columns, QObject *parent)
    : QStandardItemModel{rows, columns, parent}
{}

// Qt's model/view framework is less needy for data passing. As it can be seen
// in the docs for Qt::ItemDataRole, we mostly use just QString. Roles using
// types other than QString such as Qt::FontRole predate QML and aren't even
// mapped by default in QAbstractItemModel::roleNames(). QML promotes greater
// separation between frontend and backend so the backend shouldn't really be
// dealing with types such as QFont.
QVariant StandardItemModel::convertTopToVariant(lua_State* L)
{
    switch (lua_type(L, -1)) {
    default:
        return {};
    case LUA_TSTRING: { // for now, only QString is supported
        auto s = tostringview(L);
        return QString::fromUtf8(s.data(), s.size());
    }
    case LUA_TUSERDATA: {
        if (!lua_getmetatable(L, -1)) {
            return {};
        }
        emilua::rawgetp(L, LUA_REGISTRYINDEX, &qimage_mt_key);
        if (!lua_rawequal(L, -1, -2)) {
            lua_pop(L, 2);
            return {};
        }
        lua_pop(L, 2);
        auto img = static_cast<QImage*>(lua_touserdata(L, -1));
        return *img;
    }
    }
}

void StandardItemModel::GUIData::consumeUpdates()
{
    std::lock_guard<std::mutex> guard{updatesMtx};
    assert(syncScheduled);
    syncScheduled = false;

    for (auto& update : updates) {
        std::visit(hana::overload(
            [&](ClearUpdate&) { clear(); },
            [&](InsertItemUpdate& u) { insertRow(u.row, u.item.release()); },
            [&](InsertRowsUpdate& u) { insertRows(u.row, u.count); },
            [&](RemoveRowsUpdate& u) { removeRows(u.row, u.count); },
            [&](InsertColumnsUpdate& u) { insertColumns(u.column, u.count); },
            [&](RemoveColumnsUpdate& u) { removeColumns(u.column, u.count); },
            [&](SetItemUpdate& u) {
                setItem(u.row, u.column, u.item.release()); }
        ), update);
    }
    updates.clear();
}

void StandardItemModel::GUIData::update(
    std::lock_guard<std::mutex>&, WorkQueue* workQueue)
{
    if (syncScheduled)
        return;

    syncScheduled = true;
    auto work = [guiData=this]() { guiData->consumeUpdates(); };
    QCoreApplication::postEvent(workQueue, new WorkUnitEvent(work));
}

void StandardItemModel::clear(WorkQueue* workQueue)
{
    rowCount = columnCount = 0;
    rows.clear();

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    guiData->updates.clear();
    guiData->updates.emplace_back(GUIData::ClearUpdate{});
    guiData->update(guard, workQueue);
}

void StandardItemModel::appendItem(
    WorkQueue* workQueue, std::unique_ptr<QStandardItem> item)
{
    int row = rowCount;
    rowCount += 1;
    rows.emplace_back();

    if (columnCount == 0)
        return;

    rows.back().emplace_back(item->clone());

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    guiData->updates.emplace_back(GUIData::InsertItemUpdate{
        row, std::move(item)});
    guiData->update(guard, workQueue);
}

void StandardItemModel::insertItem(
    WorkQueue* workQueue, int row, std::unique_ptr<QStandardItem> item)
{
    rowCount += 1;
    auto& d = *rows.emplace(rows.begin() + row);

    if (columnCount == 0)
        return;

    d.emplace_back(item->clone());

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    guiData->updates.emplace_back(GUIData::InsertItemUpdate{
        row, std::move(item)});
    guiData->update(guard, workQueue);
}

void StandardItemModel::insertRows(WorkQueue* workQueue, int row, int count)
{
    if (count == 0)
        return;

    rowCount += count;
    rows.reserve(rows.size() + count);

    // inefficient, but we can't use the other insert() overloads because
    // value_type isn't CopyConstructible
    for (int i = 0 ; i != count ; ++i) {
        rows.insert(rows.begin() + (row + i), decltype(rows)::value_type{});
    }

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    guiData->updates.emplace_back(GUIData::InsertRowsUpdate{ row, count });
    guiData->update(guard, workQueue);
}

void StandardItemModel::removeRows(WorkQueue* workQueue, int row, int count)
{
    if (count == 0 || row >= rowCount)
        return;

    int maxToRemove = rows.end() - (rows.begin() + row);
    count = std::min(maxToRemove, count);

    if (count == 0)
        return;

    rowCount -= count;
    rows.erase(rows.begin() + row, rows.begin() + row + count);

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    // TODO: remove updates that cancel each-other from the queue (e.g. items
    // modified at positions that are going to be removed due to this update)
    guiData->updates.emplace_back(GUIData::RemoveRowsUpdate{ row, count });
    guiData->update(guard, workQueue);
}

void StandardItemModel::insertColumns(
    WorkQueue* workQueue, int column, int count)
{
    if (count == 0)
        return;

    columnCount += count;

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    guiData->updates.emplace_back(
        GUIData::InsertColumnsUpdate{ column, count });
    guiData->update(guard, workQueue);
}

void StandardItemModel::removeColumns(
    WorkQueue* workQueue, int column, int count)
{
    if (count == 0 || column >= columnCount)
        return;

    int maxToRemove = columnCount - column;
    count = std::min(maxToRemove, count);

    if (count == 0)
        return;

    columnCount -= count;
    for (auto& row : rows) {
        if (row.size() > columnCount)
            row.resize(columnCount);
    }

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    // TODO: remove updates that cancel each-other from the queue (e.g. items
    // modified at positions that are going to be removed due to this update)
    guiData->updates.emplace_back(GUIData::RemoveColumnsUpdate{column, count});
    guiData->update(guard, workQueue);
}

void StandardItemModel::setItem(
    WorkQueue* workQueue, int row, int column,
    std::unique_ptr<QStandardItem> item)
{
    auto& r = rows[row];
    if (!(column < r.size())) {
        r.resize(column + 1);
    }

    r[column].reset(item->clone());

    std::lock_guard<std::mutex> guard{guiData->updatesMtx};
    // TODO: remove updates that cancel each-other from the queue (e.g. previous
    // set-item at the same position)
    guiData->updates.emplace_back(GUIData::SetItemUpdate{
        row, column, std::move(item)});
    guiData->update(guard, workQueue);
}

void StandardItemModel::setRowCount(WorkQueue* workQueue, int rowCount)
{
    int diff = rowCount - this->rowCount;
    if (diff == 0) [[unlikely]] {
        return;
    } else if (diff > 0) {
        return insertRows(workQueue, this->rowCount, diff);
    } else { assert(diff < 0);
        return removeRows(workQueue, this->rowCount + diff, -diff);
    }
}

void StandardItemModel::setColumnCount(WorkQueue* workQueue, int columnCount)
{
    int diff = columnCount - this->columnCount;
    if (diff == 0) [[unlikely]] {
        return;
    } else if (diff > 0) {
        return insertColumns(workQueue, this->columnCount, diff);
    } else { assert(diff < 0);
        return removeColumns(workQueue, this->columnCount + diff, -diff);
    }
}

QStandardItem* StandardItemModel::item(int row, int column)
{
    auto& r = rows[row];
    if (!(column < r.size())) {
        return nullptr;
    }

    return r[column].get();
}

} // namespace emilua_qt
