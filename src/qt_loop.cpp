// Copyright (c) 2023, 2024, 2025 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

EMILUA_GPERF_DECLS_BEGIN(includes)
#include <QtCore/QResource>

#include <QtQml/QQmlComponent>
#include <QtQml/QQmlContext>
#include <QtQml/QQmlEngine>

#include <QtGui/QGuiApplication>

#include <emilua/file_descriptor.hpp>
#include <emilua/async_base.hpp>
#include <emilua/filesystem.hpp>
#include <emilua/byte_span.hpp>
#include <emilua/core.hpp>

#include <emilua_qt/standarditemmodel.hpp>
#include <emilua_qt/bytespanresource.hpp>
#include <emilua_qt/error_category.hpp>
#include <emilua_qt/workqueue.hpp>
#include <emilua_qt/observer.hpp>
#include <emilua_qt/image.hpp>

#include <boost/safe_numerics/safe_integer.hpp>
#include <boost/scope_exit.hpp>

#if BOOST_OS_UNIX
#include <sys/mman.h>
#endif // BOOST_OS_UNIX
EMILUA_GPERF_DECLS_END(includes)

namespace emilua_qt {

EMILUA_GPERF_DECLS_BEGIN(qt_loop)
EMILUA_GPERF_NAMESPACE(emilua_qt)
namespace hana = boost::hana;
namespace asio = boost::asio;
namespace fs = std::filesystem;

using emilua::vm_context;
using emilua::get_vm_context;
using emilua::tostringview;
using emilua::setmetatable;
using emilua::rawgetp;
using emilua::push;
using emilua::finalizer;
using emilua::byte_span_handle;
using emilua::byte_span_mt_key;
using emilua::filesystem_path_mt_key;

char qimage_mt_key;

static char qml_context_mt_key;
static char qml_context_property_mt_key;
static char qobject_mt_key;
static char standard_item_model_mt_key;

#ifndef EMILUA_QT_STATIC_BUILD
static int argc = 1;
static const char* argv[] = { "emilua" };
#endif // !defined(EMILUA_QT_STATIC_BUILD)

static QGuiApplication* app;
static WorkQueue* workQueue;
EMILUA_GPERF_DECLS_END(qt_loop)

static void on_index_resume_fiber(
    vm_context& vm_ctx, lua_State* fiber,
    const QVariant& var, const std::shared_ptr<QObject>& strong_ref
) {
    static constexpr auto opt_args = vm_context::options::arguments;

    if (!var.isValid()) {
        std::error_code ec{emilua::errc::bad_index};
        vm_ctx.fiber_resume(
            fiber,
            hana::make_set(
                vm_context::options::skip_clear_interrupter,
                hana::make_pair(
                    opt_args, hana::make_tuple(ec))));
        return;
    }

    auto push_object = [&var,&strong_ref](lua_State* L) {
        switch (var.typeId()) {
        case QMetaType::Bool:
            lua_pushboolean(L, var.toBool());
            break;
        case QMetaType::Double:
            lua_pushnumber(L, var.toDouble());
            break;
        case QMetaType::Int:
            lua_pushinteger(L, var.toInt());
            break;
        case QMetaType::QString: {
            auto s = var.toString().toUtf8();
            lua_pushlstring(L, s.data(), s.size());
            break;
        }
        case QMetaType::QUrl: {
            auto s = var.toUrl().toEncoded();
            lua_pushlstring(L, s.data(), s.size());
            break;
        }
        default:
            if (auto object = var.value<QObject*>() ; object) {
                auto ret = static_cast<std::shared_ptr<QObject>*>(
                    lua_newuserdata(L, sizeof(std::shared_ptr<QObject>))
                );
                rawgetp(L, LUA_REGISTRYINDEX, &qobject_mt_key);
                setmetatable(L, -2);
                new (ret) std::shared_ptr<QObject>{strong_ref, object};
            } else {
                lua_pushnil(L);
            }
            break;
        }
    };
    vm_ctx.fiber_resume(
        fiber,
        hana::make_set(
            vm_context::options::skip_clear_interrupter,
            hana::make_pair(
                opt_args,
                hana::make_tuple(std::nullopt, push_object))));
}

#ifdef EMILUA_QT_STATIC_BUILD
void init_loop(QGuiApplication& a)
{
    qmlRegisterType<Image>("Emilua", 1, 2, "Image");
    workQueue = new WorkQueue{app = &a};
}
#endif // defined(EMILUA_QT_STATIC_BUILD)

#ifndef EMILUA_QT_STATIC_BUILD
extern "C" BOOST_SYMBOL_EXPORT void init_loop() noexcept
{
    QCoreApplication::setAttribute(Qt::AA_PluginApplication, true);

    app = new QGuiApplication{argc, (char**)argv};
    app->setQuitOnLastWindowClosed(false);
    qmlRegisterType<Image>("Emilua", 1, 2, "Image");

    workQueue = new WorkQueue{app};
}

extern "C" BOOST_SYMBOL_EXPORT void loop_start() noexcept
{
    app->exec();

    // some QPA plugins are buggy and will randomly crash if we free resources
#if 0
    delete app;
#endif // 0
}

extern "C" BOOST_SYMBOL_EXPORT void loop_end() noexcept
{
    QMetaObject::invokeMethod(app, "exit", Qt::QueuedConnection);
}
#endif // !defined(EMILUA_QT_STATIC_BUILD)

static int load_qml(lua_State* L)
{
    static constexpr auto opt_args = vm_context::options::arguments;

    lua_settop(L, 1);

    std::string data;
    bool is_url;

    switch (lua_type(L, 1)) {
    case LUA_TUSERDATA: {
        auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
        if (!bs || !lua_getmetatable(L, 1)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }
        rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
        if (!lua_rawequal(L, -1, -2)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }
        data = static_cast<std::string_view>(*bs);
        is_url = false;
        break;
    }
    case LUA_TSTRING:
        data = tostringview(L, 1);
        is_url = true;
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,data,is_url
    ]() {
        auto engine = new QQmlEngine{app};
        auto component = new QQmlComponent{engine, app};
        QObject::connect(
            component, &QQmlComponent::statusChanged,
            [
                work_guard,vm_ctx,current_fiber,engine,component
            ](QQmlComponent::Status status) {
                if (status == QQmlComponent::Error) {
                    auto errors = [errors=component->errors()](lua_State* L) {
                        push(L, errc::failed_to_load_qml);

                        lua_pushliteral(L, "errors");
                        lua_newtable(L);
                        for (int i = 0 ; i != errors.size() ; ++i) {
                            auto e = errors[i].toString().toUtf8();
                            lua_pushlstring(L, e.data(), e.size());
                            lua_rawseti(L, -2, i + 1);
                        }
                        lua_rawset(L, -3);
                    };
                    vm_ctx->strand().post([vm_ctx,current_fiber,errors]() {
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(errors))));
                    }, std::allocator<void>{});
                    component->deleteLater();
                    engine->deleteLater();
                    return;
                }

                if (status != QQmlComponent::Ready)
                    return;

                QObject* rootObject = component->create();
                rootObject->setParent(engine);
                component->deleteLater();

                std::shared_ptr<QQmlEngine> engine2{
                    engine, [](QQmlEngine* o) { o->deleteLater(); }};
                std::shared_ptr<QQmlContext> rootContext{
                    std::move(engine2), qmlContext(rootObject)};

                vm_ctx->strand().post([vm_ctx,current_fiber,rootContext]() {
                    auto push_qml_context = [rootContext](lua_State* L) {
                        auto ret = static_cast<std::shared_ptr<QQmlContext>*>(
                            lua_newuserdata(
                                L, sizeof(std::shared_ptr<QQmlContext>))
                        );
                        rawgetp(L, LUA_REGISTRYINDEX, &qml_context_mt_key);
                        setmetatable(L, -2);
                        new (ret) std::shared_ptr<QQmlContext>{rootContext};
                    };
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter,
                            hana::make_pair(
                                opt_args,
                                hana::make_tuple(
                                    std::nullopt, push_qml_context))));
                }, std::allocator<void>{});
            });
        if (is_url) {
            component->loadUrl(
                QUrl::fromEncoded(QByteArray::fromStdString(data)));
        } else {
            component->setData(QByteArray::fromStdString(data), QUrl{});
        }
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int register_resource(lua_State* L)
{
    lua_settop(L, 2);

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    if (lua_type(L, 1) != LUA_TUSERDATA) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    std::shared_ptr<unsigned char[]> data;
    QString file_name;

    auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
    if (!bs || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
    if (lua_rawequal(L, -1, -2)) {
        data = bs->data;
    } else {
        rawgetp(L, LUA_REGISTRYINDEX, &filesystem_path_mt_key);
        if (!lua_rawequal(L, -1, -3)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }

        auto path = static_cast<fs::path*>(lua_touserdata(L, 1))->u16string();
        file_name = QString::fromUtf16(path.data(), path.size());
    }

    QString map_root;
    switch (lua_type(L, 2)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    case LUA_TNIL:
        break;
    case LUA_TSTRING: {
        auto s = tostringview(L, 2);
        map_root = QString::fromUtf8(s.data(), s.size());
    }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,data,file_name,
        map_root
    ]() {
        bool ret;
        if (data) {
            ret = QResource::registerResource(data.get(), map_root);
            if (ret) {
                auto ref = new ByteSpanResource{app};
                ref->data = data;
            }
        } else {
            ret = QResource::registerResource(file_name, map_root);
        }

        static constexpr auto opt_args = vm_context::options::arguments;

        vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter,
                    hana::make_pair(
                        opt_args, hana::make_tuple(ret))));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int unregister_resource(lua_State* L)
{
    lua_settop(L, 2);

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    if (lua_type(L, 1) != LUA_TUSERDATA) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    std::shared_ptr<unsigned char[]> data;
    QString file_name;

    auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
    if (!bs || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
    if (lua_rawequal(L, -1, -2)) {
        data = bs->data;
    } else {
        rawgetp(L, LUA_REGISTRYINDEX, &filesystem_path_mt_key);
        if (!lua_rawequal(L, -1, -3)) {
            push(L, std::errc::invalid_argument, "arg", 1);
            return lua_error(L);
        }

        auto path = static_cast<fs::path*>(lua_touserdata(L, 1))->u16string();
        file_name = QString::fromUtf16(path.data(), path.size());
    }

    QString map_root;
    switch (lua_type(L, 2)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    case LUA_TNIL:
        break;
    case LUA_TSTRING: {
        auto s = tostringview(L, 2);
        map_root = QString::fromUtf8(s.data(), s.size());
    }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,data,file_name,
        map_root
    ]() {
        bool ret;
        if (data) {
            ret = QResource::unregisterResource(data.get(), map_root);
            if (ret) {
                for (auto& o : app->children()) {
                    if (auto bsr = qobject_cast<ByteSpanResource*>(o) ; bsr) {
                        if (bsr->data == data) {
                            bsr->deleteLater();
                            break;
                        }
                    }
                }
            }
        } else {
            ret = QResource::unregisterResource(file_name, map_root);
        }

        static constexpr auto opt_args = vm_context::options::arguments;

        vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter,
                    hana::make_pair(
                        opt_args, hana::make_tuple(ret))));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int standard_item_model_new(lua_State* L)
{
    lua_settop(L, 2);

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    int rows, columns;
    switch (lua_type(L, 1)) {
    case LUA_TNIL:
        if (!lua_isnil(L, 2)) {
            push(L, std::errc::invalid_argument);
            return lua_error(L);
        }
        rows = columns = 0;
        break;
    default:
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    case LUA_TNUMBER:
        rows = lua_tointeger(L, 1);
        columns = luaL_checkinteger(L, 2);
        break;
    }

    // unfortunately std::move_only_function didn't make into C++20 so we can't
    // use std::unique_ptr
    auto model = std::make_shared<StandardItemModel>(rows, columns);

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,
        model=std::move(model)
    ]() {
        model->guiData.reset(new StandardItemModel::GUIData{
            model->rowCount, model->columnCount, app});

        model->roleNames = model->guiData->roleNames();
        const auto& names = model->roleNames;
        for (auto it = names.begin() ; it != names.end() ; ++it) {
            model->roleIds.emplace(it->toStdString(), it.key());
        }

        vm_ctx->strand().post([vm_ctx,current_fiber,model=std::move(model)]() {
            auto push_model = [&model](lua_State* L) {
                auto ret = static_cast<StandardItemModel*>(
                    lua_newuserdata(L, sizeof(StandardItemModel))
                );
                rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
                setmetatable(L, -2);
                new (ret) StandardItemModel{std::move(*model)};
            };

            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter,
                    hana::make_pair(
                        vm_context::options::arguments,
                        hana::make_tuple(std::nullopt, push_model))));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(std::move(work)));
    return lua_yield(L, 0);
}

#if BOOST_OS_UNIX
static int decode_image(lua_State* L)
{
    lua_settop(L, 2);

    auto bs = static_cast<byte_span_handle*>(lua_touserdata(L, 1));
    if (!bs || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    const char* fmt;
    switch (lua_type(L, 2)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    case LUA_TNIL:
        fmt = nullptr;
        break;
    case LUA_TSTRING:
        fmt = lua_tostring(L, 2);
        break;
    }

    auto img = QImage::fromData(bs->data.get(), bs->size, fmt);
    if (img.isNull()) {
        lua_pushliteral(L, "failed to decode image");
        return lua_error(L);
    }

    lua_createtable(L, /*narr=*/0, /*nrec=*/4);

    lua_pushliteral(L, "width");
    lua_pushinteger(L, img.width());
    lua_rawset(L, -3);

    lua_pushliteral(L, "height");
    lua_pushinteger(L, img.height());
    lua_rawset(L, -3);

    lua_pushliteral(L, "format");
    switch (img.format()) {
    case QImage::Format_RGB32:
        lua_pushliteral(L, "RGB32");
        break;
    case QImage::Format_ARGB32:
        lua_pushliteral(L, "ARGB32");
        break;
    case QImage::Format_ARGB32_Premultiplied:
        lua_pushliteral(L, "ARGB32_Premultiplied");
        break;
    default: {
        if (img.hasAlphaChannel()) {
            img = img.convertToFormat(QImage::Format_ARGB32_Premultiplied);
            lua_pushliteral(L, "ARGB32_Premultiplied");
        } else {
            img = img.convertToFormat(QImage::Format_RGB32);
            lua_pushliteral(L, "RGB32");
        }
    }
    }
    lua_rawset(L, -3);

    int mfd = -1;
    BOOST_SCOPE_EXIT_ALL(&) { if (mfd != -1) close(mfd); };
    mfd = memfd_create("emilua/qt/decode_image", MFD_ALLOW_SEALING);
    if (mfd == -1) {
        push(L, std::error_code{errno, std::system_category()});
        return lua_error(L);
    }

    if (
        auto nwritten = write(mfd, img.constBits(), img.sizeInBytes()) ;
        nwritten != img.sizeInBytes()
    ) {
        std::error_code ec;
        if (nwritten == -1) {
            ec = std::error_code{errno, std::system_category()};
        } else {
            ec = make_error_code(std::errc::invalid_argument);
        }
        push(L, ec);
        return lua_error(L);
    }

    lua_pushliteral(L, "memfd");
    {
        auto handle = static_cast<emilua::file_descriptor_handle*>(
            lua_newuserdata(L, sizeof(emilua::file_descriptor_handle))
        );
        rawgetp(L, LUA_REGISTRYINDEX, &emilua::file_descriptor_mt_key);
        setmetatable(L, -2);

        *handle = mfd;
        mfd = emilua::INVALID_FILE_DESCRIPTOR;
    }
    lua_rawset(L, -3);

    return 1;
}

static int image_from_decode_result(lua_State* L)
{
    luaL_checktype(L, 1, LUA_TTABLE);

    lua_pushliteral(L, "width");
    lua_rawget(L, 1);
    int width = luaL_checkinteger(L, -1);
    if (width <= 0) {
        push(L, std::errc::invalid_argument);
        return lua_error(L);
    }

    lua_pushliteral(L, "height");
    lua_rawget(L, 1);
    int height = luaL_checkinteger(L, -1);
    if (height <= 0) {
        push(L, std::errc::invalid_argument);
        return lua_error(L);
    }

    boost::safe_numerics::safe<std::size_t> size_in_bytes = width;
    try {
        size_in_bytes *= height;
        size_in_bytes *= 4;
    } catch (const std::exception&) {
        push(L, std::errc::invalid_argument);
        return lua_error(L);
    }

    lua_pushliteral(L, "format");
    lua_rawget(L, 1);
    luaL_checktype(L, -1, LUA_TSTRING);
    auto formatstr = tostringview(L, -1);
    QImage::Format format;
    if (formatstr == "RGB32") {
        format = QImage::Format_RGB32;
    } else if (formatstr == "ARGB32") {
        format = QImage::Format_ARGB32;
    } else if (formatstr == "ARGB32_Premultiplied") {
        format = QImage::Format_ARGB32_Premultiplied;
    } else {
        // QImage::Format_RGB888 isn't accepted because QImage's memory buffer
        // constructor requires each scanline to be 32-bit aligned.
        push(L, std::errc::invalid_argument);
        return lua_error(L);
    }

    lua_pushliteral(L, "memfd");
    lua_rawget(L, 1);
    auto handle = static_cast<emilua::file_descriptor_handle*>(
        lua_touserdata(L, -1));
    if (!handle || !lua_getmetatable(L, -1)) {
        push(L, std::errc::invalid_argument, "arg", "memfd");
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &emilua::file_descriptor_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", "memfd");
        return lua_error(L);
    }

    if (*handle == emilua::INVALID_FILE_DESCRIPTOR) {
        push(L, std::errc::device_or_resource_busy);
        return lua_error(L);
    }

    if (fcntl(*handle, F_ADD_SEALS, F_SEAL_SHRINK) == -1) {
        push(L, std::error_code{errno, std::system_category()});
        return lua_error(L);
    }

    void* mapped_region = mmap(
        /*addr=*/NULL, size_in_bytes, PROT_READ, MAP_SHARED, *handle,
        /*offset=*/0);
    if (mapped_region == MAP_FAILED) {
        push(L, std::error_code{errno, std::system_category()});
        return lua_error(L);
    }

    auto block = std::make_unique<std::pair<void*, std::size_t>>(
        mapped_region, size_in_bytes);

    QImage img{
        static_cast<const unsigned char*>(mapped_region), width, height, format,
        [](void* info) {
            auto block = static_cast<std::pair<void*, std::size_t>*>(info);
            munmap(block->first, block->second);
            delete block;
        }, block.release()};

    auto ret = static_cast<QImage*>(lua_newuserdata(L, sizeof(QImage)));
    rawgetp(L, LUA_REGISTRYINDEX, &qimage_mt_key);
    setmetatable(L, -2);
    new (ret) QImage{img};
    return 1;
}
#endif // BOOST_OS_UNIX

static int qobject_mt_index(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& object = *static_cast<std::shared_ptr<QObject>*>(
        lua_touserdata(L, 1));
    std::string key{tostringview(L, 2)};

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,key
    ]() {
        auto var = object->property(key.c_str());
        vm_ctx->strand().post([vm_ctx,current_fiber,var,object]() {
            on_index_resume_fiber(*vm_ctx, current_fiber, var, object);
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qobject_mt_newindex(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& object = *static_cast<std::shared_ptr<QObject>*>(
        lua_touserdata(L, 1));
    auto key = tostringview(L, 2);

    QVariant value;
    switch (lua_type(L, 3)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    case LUA_TBOOLEAN:
        value.setValue<bool>(lua_toboolean(L, 3));
        break;
    case LUA_TNUMBER:
        value.setValue(lua_tonumber(L, 3));
        break;
    case LUA_TSTRING:
        value.setValue<QString>(lua_tostring(L, 3));
        break;
    case LUA_TUSERDATA: {
        if (!lua_getmetatable(L, 3)) {
            push(L, std::errc::invalid_argument, "arg", 3);
            return lua_error(L);
        }
        rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
        if (lua_rawequal(L, -1, -2)) {
            auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 3));
            QAbstractItemModel* interface = model->guiData.get();
            value.setValue(interface);
        } else {
            rawgetp(L, LUA_REGISTRYINDEX, &qimage_mt_key);
            if (lua_rawequal(L, -1, -3)) {
                auto img = static_cast<QImage*>(lua_touserdata(L, 3));
                value.setValue(*img);
            } else {
                push(L, std::errc::invalid_argument, "arg", 3);
                return lua_error(L);
            }
        }
        break;
    }
    case LUA_TNIL: {
        auto work = [
            work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,
            key=std::string{key}
        ]() {
            for (auto& o : object->children()) {
                if (auto observer = qobject_cast<Observer*>(o) ; observer) {
                    if (observer->subscribedSignal == key)
                        observer->deleteLater();
                }
            }

            if (object->property(key.c_str()).isValid()) {
                object->setProperty(key.c_str(), QVariant{});
            }

            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
        };
        app->postEvent(workQueue, new WorkUnitEvent(work));
        return lua_yield(L, 0);
    }
    case LUA_TFUNCTION: {
        // QObject::metaObject() is not officially thread-safe. However we deem
        // it as so here. The metaobject cannot change during the object
        // lifetime. The metaobject is just a pointer to a static variable. It
        // makes no sense to assume otherwise. QObject::staticMetaObject exists,
        // but it's not usable here.
        auto metaObject = object->metaObject();
        auto signalIdx = metaObject->indexOfSignal(
            QMetaObject::normalizedSignature(key.data()));
        if (signalIdx == -1) {
            push(L, emilua::errc::bad_index, "index", 2);
            return lua_error(L);
        }
        auto signal = metaObject->method(signalIdx);

        lua_pushvalue(L, 3);
        int function = luaL_ref(L, LUA_REGISTRYINDEX);

        auto observer = new Observer{vm_ctx, function};
        observer->subscribedSignal = key;
        observer->moveToThread(app->thread());

        auto work = [
            work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,
            observer,key=std::string{key},signal
        ]() {
            for (auto& o : object->children()) {
                if (auto observer = qobject_cast<Observer*>(o) ; observer) {
                    if (observer->subscribedSignal == key)
                        observer->deleteLater();
                }
            }

            observer->setParent(object.get());

            auto metaObject = observer->metaObject();
            QMetaMethod genericSlot;

            for (int i = 0 ; i != metaObject->methodCount() ; ++i) {
                auto slot = metaObject->method(i);
                if (slot.methodType() != QMetaMethod::Slot ||
                    slot.name() != "map") {
                    continue;
                }

                if (slot.parameterCount() == 0) {
                    genericSlot = slot;
                }

                if (
                    (
                        signal.parameterCount() > 0 &&
                        slot.parameterCount() == 0
                    ) ||
                    !QMetaObject::checkConnectArgs(signal, slot)
                ) {
                    continue;
                }

                QObject::connect(object.get(), signal, observer, slot);
                vm_ctx->strand().post([vm_ctx,current_fiber]() {
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter));
                }, std::allocator<void>{});
                return;
            }

            QObject::connect(object.get(), signal, observer, genericSlot);
            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
        };
        app->postEvent(workQueue, new WorkUnitEvent(work));
        return lua_yield(L, 0);
    }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,
        key=std::string{key},value
    ]() {
        object->setProperty(key.c_str(), value);

        vm_ctx->strand().post([vm_ctx,current_fiber]() {
            vm_ctx->fiber_resume(
                current_fiber,
                hana::make_set(
                    vm_context::options::skip_clear_interrupter));
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qobject_mt_call(lua_State* L)
{
    static constexpr auto opt_args = vm_context::options::arguments;

    int nargs = lua_gettop(L);
    if (nargs < 2 || lua_type(L, 2) != LUA_TSTRING) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    if (nargs > 2 + 10) {
        push(L, errc::too_many_arguments);
        return lua_error(L);
    }

    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& object = *static_cast<std::shared_ptr<QObject>*>(
        lua_touserdata(L, 1));
    std::string method{tostringview(L, 2)};

    QVariantList arguments;
    for (int i = 3 ; i <= nargs ; ++i) {
        switch (lua_type(L, i)) {
        case LUA_TNIL:
            arguments.emplace_back();
            arguments.back().setValue<std::nullptr_t>(nullptr);
            break;
        case LUA_TBOOLEAN:
            arguments.emplace_back(static_cast<bool>(lua_toboolean(L, i)));
            break;
        case LUA_TNUMBER:
            arguments.emplace_back(lua_tonumber(L, i));
            break;
        case LUA_TSTRING: {
            auto s = tostringview(L, i);
            arguments.emplace_back(QString::fromUtf8(s.data(), s.size()));
            break;
        }
        default:
            push(L, std::errc::invalid_argument, "arg", i);
            return lua_error(L);
        }
    }

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,object,method,
        arguments
    ]() {
        auto metaObject = object->metaObject();
        int indexOfMethod = metaObject->indexOfMethod(
            QMetaObject::normalizedSignature(method.c_str()));
        if (indexOfMethod == -1) {
            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                std::error_code ec{emilua::errc::bad_index};
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter,
                        hana::make_pair(opt_args, hana::make_tuple(ec))));
            }, std::allocator<void>{});
            return;
        }
        auto metaMethod = metaObject->method(indexOfMethod);
        if (metaMethod.parameterCount() != arguments.size()) {
            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                std::error_code ec{errc::wrong_signature};
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter,
                        hana::make_pair(opt_args, hana::make_tuple(ec))));
            }, std::allocator<void>{});
            return;
        }

        std::array<std::decay_t<decltype(Q_ARG(int, 0))>, 10> arguments2;
        assert(arguments2.size() >= arguments.size());

        std::array<bool, 10> boolBuffer;
        std::array<double, 10> doubleBuffer;
        std::array<int, 10> intBuffer;
        std::array<QString, 10> stringBuffer;
        std::array<QUrl, 10> urlBuffer;

        for (int i = 0 ; i != arguments.size() ; ++i) {
            switch (arguments[i].typeId()) {
            case QMetaType::Nullptr:
                if (metaMethod.parameterType(i) != QMetaType::QVariant) {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                arguments2[i] = Q_ARG(QVariant, arguments[i]);
                break;
            case QMetaType::Bool:
                switch (metaMethod.parameterType(i)) {
                case QMetaType::Bool:
                    boolBuffer[i] = arguments[i].toBool();
                    arguments2[i] = Q_ARG(bool, boolBuffer[i]);
                    break;
                case QMetaType::QVariant:
                    arguments2[i] = Q_ARG(QVariant, arguments[i]);
                    break;
                default: {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                }
                break;
            case QMetaType::Double:
                switch (metaMethod.parameterType(i)) {
                case QMetaType::Double:
                    doubleBuffer[i] = arguments[i].toDouble();
                    arguments2[i] = Q_ARG(double, doubleBuffer[i]);
                    break;
                case QMetaType::Int:
                    intBuffer[i] = arguments[i].toDouble();
                    arguments2[i] = Q_ARG(int, intBuffer[i]);
                    break;
                case QMetaType::QVariant:
                    arguments2[i] = Q_ARG(QVariant, arguments[i]);
                    break;
                default: {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                }
                break;
            case QMetaType::QString:
                switch (metaMethod.parameterType(i)) {
                case QMetaType::QString:
                    stringBuffer[i] = arguments[i].toString();
                    arguments2[i] = Q_ARG(QString, stringBuffer[i]);
                    break;
                case QMetaType::QUrl:
                    urlBuffer[i] = arguments[i].toString();
                    arguments2[i] = Q_ARG(QUrl, urlBuffer[i]);
                    break;
                case QMetaType::QVariant:
                    arguments2[i] = Q_ARG(QVariant, arguments[i]);
                    break;
                default: {
                    vm_ctx->strand().post([vm_ctx,current_fiber]() {
                        std::error_code ec{errc::wrong_signature};
                        vm_ctx->fiber_resume(
                            current_fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(ec))));
                    }, std::allocator<void>{});
                    return;
                }
                }
                break;
            default:
                qFatal("internal error... shouldn't have invalid type stored "
                       "here");
                std::abort();
                break;
            }
        }

        auto dispatchWithReturn = [&](auto type) {
            typename decltype(type)::type retBuffer;
            switch (arguments.size()) {
            case 0:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer));
                break;
            case 1:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0]);
                break;
            case 2:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1]);
                break;
            case 3:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2]);
                break;
            case 4:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3]);
                break;
            case 5:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4]);
                break;
            case 6:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5]);
                break;
            case 7:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6]);
                break;
            case 8:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7]);
                break;
            case 9:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8]);
                break;
            case 10:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection, qReturnArg(retBuffer),
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8], arguments2[9]);
                break;
            default:
                vm_ctx->strand().post([vm_ctx,current_fiber]() {
                    std::error_code ec{errc::too_many_arguments};
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter,
                            hana::make_pair(opt_args, hana::make_tuple(ec))));
                }, std::allocator<void>{});
                return;
            }

            QVariant ret = retBuffer;
            vm_ctx->strand().post([vm_ctx,current_fiber,ret]() {
                auto push_ret = [&ret](lua_State* L) {
                    if (!ret.isValid()) {
                        lua_pushnil(L);
                        return;
                    }

                    switch (ret.typeId()) {
                    case QMetaType::Nullptr:
                        lua_pushnil(L);
                        break;
                    case QMetaType::Bool:
                        lua_pushboolean(L, ret.toBool());
                        break;
                    case QMetaType::Double:
                        lua_pushnumber(L, ret.toDouble());
                        break;
                    case QMetaType::Int:
                        lua_pushinteger(L, ret.toInt());
                        break;
                    case QMetaType::QString: {
                        auto s = ret.toString().toUtf8();
                        lua_pushlstring(L, s.data(), s.size());
                        break;
                    }
                    case QMetaType::QUrl: {
                        auto s = ret.toUrl().toEncoded();
                        lua_pushlstring(L, s.data(), s.size());
                        break;
                    }
                    default:
                        lua_pushnil(L);
                    }
                };
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter,
                        hana::make_pair(
                            opt_args,
                            hana::make_tuple(std::nullopt, push_ret))));
            }, std::allocator<void>{});
        };

        switch (metaMethod.returnType()) {
        default:
        case QMetaType::Void:
            switch (arguments.size()) {
            case 0:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection);
                break;
            case 1:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0]);
                break;
            case 2:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1]);
                break;
            case 3:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2]);
                break;
            case 4:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3]);
                break;
            case 5:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4]);
                break;
            case 6:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5]);
                break;
            case 7:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6]);
                break;
            case 8:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7]);
                break;
            case 9:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8]);
                break;
            case 10:
                metaMethod.invoke(
                    object.get(), Qt::DirectConnection,
                    arguments2[0], arguments2[1], arguments2[2], arguments2[3],
                    arguments2[4], arguments2[5], arguments2[6], arguments2[7],
                    arguments2[8], arguments2[9]);
                break;
            default:
                vm_ctx->strand().post([vm_ctx,current_fiber]() {
                    std::error_code ec{errc::too_many_arguments};
                    vm_ctx->fiber_resume(
                        current_fiber,
                        hana::make_set(
                            vm_context::options::skip_clear_interrupter,
                            hana::make_pair(opt_args, hana::make_tuple(ec))));
                }, std::allocator<void>{});
                return;
            }

            vm_ctx->strand().post([vm_ctx,current_fiber]() {
                vm_ctx->fiber_resume(
                    current_fiber,
                    hana::make_set(
                        vm_context::options::skip_clear_interrupter));
            }, std::allocator<void>{});
            return;
        case QMetaType::QVariant:
            return dispatchWithReturn(hana::type_c<QVariant>);
        case QMetaType::Bool:
            return dispatchWithReturn(hana::type_c<bool>);
        case QMetaType::Double:
            return dispatchWithReturn(hana::type_c<double>);
        case QMetaType::Int:
            return dispatchWithReturn(hana::type_c<int>);
        case QMetaType::QString:
            return dispatchWithReturn(hana::type_c<QString>);
        case QMetaType::QUrl:
            return dispatchWithReturn(hana::type_c<QUrl>);
        }
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

static int qml_context_mt_index(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& context = *static_cast<std::shared_ptr<QQmlContext>*>(
        lua_touserdata(L, 1));
    auto key = tostringview(L, 2);

    return EMILUA_GPERF_BEGIN(key)
        EMILUA_GPERF_PARAM(
            int (*action)(
                lua_State*, std::shared_ptr<vm_context>&, lua_State*,
                std::shared_ptr<QQmlContext>&))
        EMILUA_GPERF_DEFAULT_VALUE(
            [](lua_State* L, std::shared_ptr<vm_context>&, lua_State*,
               std::shared_ptr<QQmlContext>&) {
                push(L, emilua::errc::bad_index, "index", 2);
                return lua_error(L);
            }
        )
        EMILUA_GPERF_PAIR(
            "property",
            [](lua_State* L, std::shared_ptr<vm_context>&,
               lua_State*, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;

                auto ret = static_cast<std::shared_ptr<QQmlContext>*>(
                    lua_newuserdata(L, sizeof(std::shared_ptr<QQmlContext>)));
                rawgetp(L, LUA_REGISTRYINDEX, &qml_context_property_mt_key);
                setmetatable(L, -2);
                new (ret) std::shared_ptr<QQmlContext>{context};
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "properties",
            [](lua_State* L, std::shared_ptr<vm_context>&,
               lua_State*, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;

                auto ret = static_cast<std::shared_ptr<QQmlContext>*>(
                    lua_newuserdata(L, sizeof(std::shared_ptr<QQmlContext>)));
                rawgetp(L, LUA_REGISTRYINDEX, &qml_context_property_mt_key);
                setmetatable(L, -2);
                new (ret) std::shared_ptr<QQmlContext>{context};
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "props",
            [](lua_State* L, std::shared_ptr<vm_context>&,
               lua_State*, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;

                auto ret = static_cast<std::shared_ptr<QQmlContext>*>(
                    lua_newuserdata(L, sizeof(std::shared_ptr<QQmlContext>)));
                rawgetp(L, LUA_REGISTRYINDEX, &qml_context_property_mt_key);
                setmetatable(L, -2);
                new (ret) std::shared_ptr<QQmlContext>{context};
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "object",
            [](lua_State* L, std::shared_ptr<vm_context>& vm_ctx,
               lua_State* fiber, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;
                static constexpr auto opt_args = vm_context::options::arguments;

                auto work = [
                    work_guard=vm_ctx->work_guard(),vm_ctx,fiber,context
                ]() {
                    std::shared_ptr<QObject> object{
                        context, context->contextObject()};
                    vm_ctx->strand().post([vm_ctx,fiber,object]() {
                        auto push_object = [&object](lua_State* L) {
                            auto ret = static_cast<std::shared_ptr<QObject>*>(
                                lua_newuserdata(
                                    L, sizeof(std::shared_ptr<QObject>)));
                            rawgetp(L, LUA_REGISTRYINDEX, &qobject_mt_key);
                            setmetatable(L, -2);
                            new (ret) std::shared_ptr<QObject>{object};
                            return 1;
                        };
                        vm_ctx->fiber_resume(
                            fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(push_object))));
                    }, std::allocator<void>{});
                };
                app->postEvent(workQueue, new WorkUnitEvent(work));
                return lua_yield(L, 0);
            })
        EMILUA_GPERF_PAIR(
            "engine",
            [](lua_State* L, std::shared_ptr<vm_context>& vm_ctx,
               lua_State* fiber, std::shared_ptr<QQmlContext>& context) {
                using namespace emilua_qt;
                static constexpr auto opt_args = vm_context::options::arguments;

                auto work = [
                    work_guard=vm_ctx->work_guard(),vm_ctx,fiber,context
                ]() {
                    std::shared_ptr<QObject> object{context, context->engine()};
                    vm_ctx->strand().post([vm_ctx,fiber,object]() {
                        auto push_object = [&object](lua_State* L) {
                            auto ret = static_cast<std::shared_ptr<QObject>*>(
                                lua_newuserdata(
                                    L, sizeof(std::shared_ptr<QObject>)));
                            rawgetp(L, LUA_REGISTRYINDEX, &qobject_mt_key);
                            setmetatable(L, -2);
                            new (ret) std::shared_ptr<QObject>{object};
                            return 1;
                        };
                        vm_ctx->fiber_resume(
                            fiber,
                            hana::make_set(
                                vm_context::options::skip_clear_interrupter,
                                hana::make_pair(
                                    opt_args, hana::make_tuple(push_object))));
                    }, std::allocator<void>{});
                };
                app->postEvent(workQueue, new WorkUnitEvent(work));
                return lua_yield(L, 0);
            })
    EMILUA_GPERF_END(key)(L, vm_ctx, current_fiber, context);
}

static int qml_context_property_mt_index(lua_State* L)
{
    auto vm_ctx = get_vm_context(L).shared_from_this();
    auto current_fiber = vm_ctx->current_fiber();
    EMILUA_CHECK_SUSPEND_ALLOWED(*vm_ctx, L);

    auto& context = *static_cast<std::shared_ptr<QQmlContext>*>(
        lua_touserdata(L, 1));
    auto key = tostringview(L, 2);

    auto work = [
        work_guard=vm_ctx->work_guard(),vm_ctx,current_fiber,context,
        key=QString::fromUtf8(key.data(), key.size())
    ]() {
        auto var = context->contextProperty(key);
        vm_ctx->strand().post([vm_ctx,current_fiber,var,context]() {
            on_index_resume_fiber(*vm_ctx, current_fiber, var, context);
        }, std::allocator<void>{});
    };
    app->postEvent(workQueue, new WorkUnitEvent(work));
    return lua_yield(L, 0);
}

EMILUA_GPERF_DECLS_BEGIN(standard_item_model)
static int standard_item_model_set_role_names(lua_State* L)
{
    using namespace emilua_qt;
    luaL_checktype(L, 2, LUA_TTABLE);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    if (model->roleNamesAreSet) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    std::vector<std::string_view> new_roles;
    for (int i = 1 ;; ++i) {
        lua_rawgeti(L, 2, i);
        switch (lua_type(L, -1)) {
        default:
            push(L, std::errc::invalid_argument, "arg", 2);
            return lua_error(L);
        case LUA_TNIL:
            lua_pop(L, 1);
            goto end_for;
        case LUA_TSTRING:
            break;
        }

        auto s = tostringview(L);
        if (model->roleIds.contains(s)) {
            push(L, std::errc::invalid_argument, "arg", 2);
            return lua_error(L);
        }
        new_roles.push_back(s);
    }
 end_for:

    model->roleNamesAreSet = true;

    for (decltype(new_roles)::size_type i = 0 ; i != new_roles.size() ; ++i) {
        // technically we don't need +1 here, but things such as
        // QStandardItem::setData() already do it and there's no harm on sharing
        // the same practice just to be good neighbours
        int id = Qt::UserRole + 1 + i;

        auto name = new_roles[i];
        model->roleNames.emplace(id, QByteArray(name.data(), name.size()));
        model->roleIds.emplace(name, id);
    }

    auto work = [roleNames=model->roleNames,guiData=model->guiData.get()]() {
        guiData->setItemRoleNames(roleNames);
    };
    app->postEvent(workQueue, new WorkUnitEvent(std::move(work)));
    return 0;
}

static int standard_item_model_clear(lua_State* L)
{
    using namespace emilua_qt;

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    model->clear(workQueue);
    return 0;
}

static int standard_item_model_append_item(lua_State* L)
{
    using namespace emilua_qt;
    luaL_checktype(L, 2, LUA_TTABLE);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    auto item = std::make_unique<QStandardItem>();
    lua_pushnil(L);
    while (lua_next(L, 2) != 0) {
        if (lua_type(L, -2) != LUA_TSTRING) {
            push(L, std::errc::invalid_argument, "arg", 2);
            return lua_error(L);
        }
        int role;
        {
            auto it = model->roleIds.find(tostringview(L, -2));
            if (it == model->roleIds.end()) {
                push(L, std::errc::invalid_argument, "arg", 2);
                return lua_error(L);
            }
            role = it->second;
        }

        if (auto v = StandardItemModel::convertTopToVariant(L) ; v.isValid()) {
            item->setData(std::move(v), role);
        } else {
            push(L, std::errc::invalid_argument, "arg", 2);
            return lua_error(L);
        }
        lua_pop(L, 1);
    }

    model->appendItem(workQueue, std::move(item));
    return 0;
}

static int standard_item_model_insert_item(lua_State* L)
{
    using namespace emilua_qt;
    luaL_checktype(L, 3, LUA_TTABLE);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int row = luaL_checknumber(L, 2);
    if (row < 0 || row > model->rowCount) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    auto item = std::make_unique<QStandardItem>();
    lua_pushnil(L);
    while (lua_next(L, 3) != 0) {
        if (lua_type(L, -2) != LUA_TSTRING) {
            push(L, std::errc::invalid_argument, "arg", 3);
            return lua_error(L);
        }
        int role;
        {
            auto it = model->roleIds.find(tostringview(L, -2));
            if (it == model->roleIds.end()) {
                push(L, std::errc::invalid_argument, "arg", 3);
                return lua_error(L);
            }
            role = it->second;
        }

        if (auto v = StandardItemModel::convertTopToVariant(L) ; v.isValid()) {
            item->setData(std::move(v), role);
        } else {
            push(L, std::errc::invalid_argument, "arg", 3);
            return lua_error(L);
        }
        lua_pop(L, 1);
    }

    model->insertItem(workQueue, row, std::move(item));
    return 0;
}

static int standard_item_model_insert_rows(lua_State* L)
{
    using namespace emilua_qt;
    lua_settop(L, 3);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int row = luaL_checknumber(L, 2);
    if (row < 0 || row > model->rowCount) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    int count = luaL_checknumber(L, 3);
    if (count < 0) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    model->insertRows(workQueue, row, count);
    return 0;
}

static int standard_item_model_remove_rows(lua_State* L)
{
    using namespace emilua_qt;
    lua_settop(L, 3);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int row = luaL_checknumber(L, 2);
    if (row < 0) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    int count = luaL_checknumber(L, 3);
    if (count < 0) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    model->removeRows(workQueue, row, count);
    return 0;
}

static int standard_item_model_insert_columns(lua_State* L)
{
    using namespace emilua_qt;
    lua_settop(L, 3);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int column = luaL_checknumber(L, 2);
    if (column < 0 || column > model->columnCount) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    int count = luaL_checknumber(L, 3);
    if (count < 0) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    model->insertColumns(workQueue, column, count);
    return 0;
}

static int standard_item_model_remove_columns(lua_State* L)
{
    using namespace emilua_qt;
    lua_settop(L, 3);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int column = luaL_checknumber(L, 2);
    if (column < 0) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    int count = luaL_checknumber(L, 3);
    if (count < 0) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    model->removeColumns(workQueue, column, count);
    return 0;
}

static int standard_item_model_set_item(lua_State* L)
{
    using namespace emilua_qt;
    luaL_checktype(L, 4, LUA_TTABLE);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int row = luaL_checknumber(L, 2);
    if (row < 0 || row >= model->rowCount) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    int column = luaL_checknumber(L, 3);
    if (column < 0 || column >= model->columnCount) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    auto item = std::make_unique<QStandardItem>();
    lua_pushnil(L);
    while (lua_next(L, 4) != 0) {
        if (lua_type(L, -2) != LUA_TSTRING) {
            push(L, std::errc::invalid_argument, "arg", 4);
            return lua_error(L);
        }
        int role;
        {
            auto it = model->roleIds.find(tostringview(L, -2));
            if (it == model->roleIds.end()) {
                push(L, std::errc::invalid_argument, "arg", 4);
                return lua_error(L);
            }
            role = it->second;
        }

        if (auto v = StandardItemModel::convertTopToVariant(L) ; v.isValid()) {
            item->setData(std::move(v), role);
        } else {
            push(L, std::errc::invalid_argument, "arg", 4);
            return lua_error(L);
        }
        lua_pop(L, 1);
    }

    model->setItem(workQueue, row, column, std::move(item));
    return 0;
}

static int standard_item_model_item(lua_State* L)
{
    using namespace emilua_qt;
    lua_settop(L, 3);

    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    if (!model || !lua_getmetatable(L, 1)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }
    rawgetp(L, LUA_REGISTRYINDEX, &standard_item_model_mt_key);
    if (!lua_rawequal(L, -1, -2)) {
        push(L, std::errc::invalid_argument, "arg", 1);
        return lua_error(L);
    }

    int row = luaL_checknumber(L, 2);
    if (row < 0 || row >= model->rowCount) {
        push(L, std::errc::invalid_argument, "arg", 2);
        return lua_error(L);
    }

    int column;
    switch (lua_type(L, 3)) {
    default:
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    case LUA_TNIL:
        column = 0;
        break;
    case LUA_TNUMBER:
        column = lua_tointeger(L, 3);
        break;
    }
    if (column < 0 || column >= model->columnCount) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    auto item = model->item(row, column);
    if (!item) {
        lua_pushnil(L);
        return 1;
    }

    lua_newtable(L);
    const auto& roleNames = model->roleNames;
    for (auto it = roleNames.begin() ; it != roleNames.end() ; ++it) {
        auto v = item->data(it.key());
        if (!v.isValid())
            continue;

        std::string_view name(it->data(), it->size());
        push(L, name);
        switch (v.typeId()) {
        default:
            lua_pushnil(L);
            break;
        case qMetaTypeId<QString>():
            push(L, v.toString().toStdString());
            break;
        }
        lua_rawset(L, -3);
    }
    return 1;
}

inline int standard_item_model_row_count_get(lua_State* L)
{
    using namespace emilua_qt;
    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    lua_pushnumber(L, model->rowCount);
    return 1;
}

inline int standard_item_model_column_count_get(lua_State* L)
{
    using namespace emilua_qt;
    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));
    lua_pushnumber(L, model->columnCount);
    return 1;
}
EMILUA_GPERF_DECLS_END(standard_item_model)

static int standard_item_model_mt_index(lua_State* L)
{
    auto key = tostringview(L, 2);
    return EMILUA_GPERF_BEGIN(key)
        EMILUA_GPERF_PARAM(int (*action)(lua_State*))
        EMILUA_GPERF_DEFAULT_VALUE([](lua_State* L) -> int {
            push(L, emilua::errc::bad_index, "index", 2);
            return lua_error(L);
        })
        EMILUA_GPERF_PAIR(
            "set_role_names",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_set_role_names);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "clear",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_clear);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "append_item",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_append_item);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "insert_item",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_insert_item);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "insert_rows",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_insert_rows);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "remove_rows",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_remove_rows);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "insert_columns",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_insert_columns);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "remove_columns",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_remove_columns);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "set_item",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_set_item);
                return 1;
            })
        EMILUA_GPERF_PAIR(
            "item",
            [](lua_State* L) -> int {
                lua_pushcfunction(L, standard_item_model_item);
                return 1;
            })
        EMILUA_GPERF_PAIR("row_count", standard_item_model_row_count_get)
        EMILUA_GPERF_PAIR("column_count", standard_item_model_column_count_get)
    EMILUA_GPERF_END(key)(L);
}

EMILUA_GPERF_DECLS_BEGIN(standard_item_model)
inline int standard_item_model_row_count_set(lua_State* L)
{
    using namespace emilua_qt;
    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));

    int rows = luaL_checknumber(L, 3);
    if (rows < 0) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    model->setRowCount(workQueue, rows);
    return 0;
}

inline int standard_item_model_column_count_set(lua_State* L)
{
    using namespace emilua_qt;
    auto model = static_cast<StandardItemModel*>(lua_touserdata(L, 1));

    int columns = luaL_checknumber(L, 3);
    if (columns < 0) {
        push(L, std::errc::invalid_argument, "arg", 3);
        return lua_error(L);
    }

    model->setColumnCount(workQueue, columns);
    return 0;
}
EMILUA_GPERF_DECLS_END(standard_item_model)

static int standard_item_model_mt_newindex(lua_State* L)
{
    auto key = tostringview(L, 2);
    return EMILUA_GPERF_BEGIN(key)
        EMILUA_GPERF_PARAM(int (*action)(lua_State*))
        EMILUA_GPERF_DEFAULT_VALUE([](lua_State* L) -> int {
            push(L, emilua::errc::bad_index, "index", 2);
            return lua_error(L);
        })
        EMILUA_GPERF_PAIR("row_count", standard_item_model_row_count_set)
        EMILUA_GPERF_PAIR("column_count", standard_item_model_column_count_set)
    EMILUA_GPERF_END(key)(L);
}

EMILUA_GPERF_DECLS_BEGIN(qimage)
inline int qimage_width(lua_State* L)
{
    auto img = static_cast<QImage*>(lua_touserdata(L, 1));
    lua_pushnumber(L, img->width());
    return 1;
}

inline int qimage_height(lua_State* L)
{
    auto img = static_cast<QImage*>(lua_touserdata(L, 1));
    lua_pushnumber(L, img->height());
    return 1;
}
EMILUA_GPERF_DECLS_END(qimage)

static int qimage_mt_index(lua_State* L)
{
    auto key = tostringview(L, 2);
    return EMILUA_GPERF_BEGIN(key)
        EMILUA_GPERF_PARAM(int (*action)(lua_State*))
        EMILUA_GPERF_DEFAULT_VALUE([](lua_State* L) -> int {
            push(L, emilua::errc::bad_index, "index", 2);
            return lua_error(L);
        })
        EMILUA_GPERF_PAIR("width", qimage_width)
        EMILUA_GPERF_PAIR("height", qimage_height)
    EMILUA_GPERF_END(key)(L);
}

#ifndef EMILUA_QT_STATIC_BUILD
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern "C" BOOST_SYMBOL_EXPORT std::error_code init_lua_module(lua_State* L);
#pragma clang diagnostic pop
#endif // !defined(EMILUA_QT_STATIC_BUILD)

std::error_code init_lua_module(lua_State* L)
{
    lua_createtable(L, /*narr=*/0, /*nrec=*/6);
    {
        lua_pushliteral(L, "load_qml");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, load_qml);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "register_resource");
        lua_pushcfunction(L, register_resource);
        lua_rawset(L, -3);

        lua_pushliteral(L, "unregister_resource");
        lua_pushcfunction(L, unregister_resource);
        lua_rawset(L, -3);

        lua_pushliteral(L, "standard_item_model");
        lua_createtable(L, /*narr=*/0, /*nrec=*/1);
        {
            lua_pushliteral(L, "new");
            {
                emilua::rawgetp(
                    L, LUA_REGISTRYINDEX,
                    &emilua::var_args__retval1_to_error__fwd_retval2__key);
                rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
                lua_pushcfunction(L, standard_item_model_new);
                lua_call(L, 2, 1);
            }
            lua_rawset(L, -3);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "decode_image");
#if BOOST_OS_UNIX
        lua_pushcfunction(L, decode_image);
#else // BOOST_OS_UNIX
        lua_pushcfunction(L, emilua::throw_enosys);
#endif // BOOST_OS_UNIX
        lua_rawset(L, -3);

        lua_pushliteral(L, "image_from_decode_result");
#if BOOST_OS_UNIX
        lua_pushcfunction(L, image_from_decode_result);
#else // BOOST_OS_UNIX
        lua_pushcfunction(L, emilua::throw_enosys);
#endif // BOOST_OS_UNIX
        lua_rawset(L, -3);
    }

    lua_pushlightuserdata(L, &qml_context_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.qml_context");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        lua_pushcfunction(L, qml_context_mt_index);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<std::shared_ptr<QQmlContext>>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    lua_pushlightuserdata(L, &qml_context_property_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.qml_context.property");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, qml_context_property_mt_index);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<std::shared_ptr<QQmlContext>>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    lua_pushlightuserdata(L, &qobject_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/4);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.object");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, qobject_mt_index);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__newindex");
        lua_pushcfunction(L, qobject_mt_newindex);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__call");
        {
            emilua::rawgetp(
                L, LUA_REGISTRYINDEX,
                &emilua::var_args__retval1_to_error__fwd_retval2__key);
            rawgetp(L, LUA_REGISTRYINDEX, &emilua::raw_error_key);
            lua_pushcfunction(L, qobject_mt_call);
            lua_call(L, 2, 1);
        }
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<std::shared_ptr<QObject>>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    lua_pushlightuserdata(L, &standard_item_model_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/4);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.standard_item_model");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        lua_pushcfunction(L, standard_item_model_mt_index);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__newindex");
        lua_pushcfunction(L, standard_item_model_mt_newindex);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<StandardItemModel>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    lua_pushlightuserdata(L, &qimage_mt_key);
    lua_createtable(L, /*narr=*/0, /*nrec=*/3);
    {
        lua_pushliteral(L, "__metatable");
        lua_pushliteral(L, "qt6.image");
        lua_rawset(L, -3);

        lua_pushliteral(L, "__index");
        lua_pushcfunction(L, qimage_mt_index);
        lua_rawset(L, -3);

        lua_pushliteral(L, "__gc");
        lua_pushcfunction(L, finalizer<QImage>);
        lua_rawset(L, -3);
    }
    lua_rawset(L, LUA_REGISTRYINDEX);

    return {};
}

} // namespace emilua_qt
