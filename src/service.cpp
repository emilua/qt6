// Copyright (c) 2023 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <emilua_qt/service.hpp>

namespace emilua_qt {

namespace asio = boost::asio;

boost::asio::io_context::id service::id;

service::service(asio::execution_context& ctx,
                 std::shared_ptr<qt_handle> handle)
    : asio::execution_context::service{ctx}
    , handle{std::move(handle)}
{}

void service::shutdown() noexcept
{
    // Body empty. We want to stop the Qt event loop after every Lua VM dies,
    // not before. Lua VM destruction will trigger posting of new events to the
    // Qt loop, so deferring Qt's loop exit  until ~service() is a good idea.
}

} // namespace emilua_qt
