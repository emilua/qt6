// Copyright (c) 2025 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <emilua_qt/image.hpp>

#include <QtGui/QPainter>

namespace emilua_qt {

Image::Image(QQuickItem *parent)
    : QQuickPaintedItem{parent}
{}

QVariant Image::image() const
{
    return image_;
}

bool Image::setImage(QVariant image)
{
    if (image.typeId() != QMetaType::QImage)
        return false;

    image_ = image.value<QImage>();
    update();
    emit imageChanged();
    return true;
}

QSize Image::sourceSize() const
{
    return image_.size();
}

void Image::paint(QPainter* painter)
{
    QRectF target(0, 0, width(), height());
    QRectF source(0, 0, image_.width(), image_.height());
    painter->drawImage(target, image_, source);
}

} // namespace emilua_qt
