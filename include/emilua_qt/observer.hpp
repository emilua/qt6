// Copyright (c) 2023 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#pragma once

#include <QtCore/QObject>
#include <QtQml/QQmlError>

#include <emilua/core.hpp>

namespace emilua_qt {

class Observer : public QObject
{
    Q_OBJECT
public:
    explicit Observer(std::shared_ptr<emilua::vm_context> vm_ctx,
                      int function, QObject *parent = nullptr);
    ~Observer();

    std::shared_ptr<emilua::vm_context> vm_ctx;
    int function;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type>
    work_guard;

    std::string subscribedSignal;

public slots:
    void map();
    void map(bool);
    void map(double);
    void map(int);
    void map(const QString&);
    void map(const QUrl&);
    void map(const QVariant&);

    void onEngineExit(int retCode);
    void onEngineQuit();
    void onEngineWarnings(const QList<QQmlError> &warnings);
};

} // namespace emilua_qt
