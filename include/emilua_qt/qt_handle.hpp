// Copyright (c) 2023 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#pragma once

#include <thread>

#include <emilua/core.hpp>

namespace emilua_qt {

struct qt_handle
{
    qt_handle();
    ~qt_handle();

    std::error_code (*const init_lua_module)(lua_State* L);

private:
    std::thread thread;
    void (*const loop_end)();
};

} // namespace emilua_qt
