// Copyright (c) 2025 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#include <emilua/native_module.hpp>

namespace emilua_qt {

class module_ : public emilua::native_module
{
public:
    std::error_code init_lua_module(
        std::shared_lock<std::shared_mutex>&,
        emilua::vm_context& vm_ctx, lua_State* L) override;
};

} // namespace emilua_qt

namespace emilua::main {

void run(app_context& appctx, asio::io_context& ioctx);

} // namespace emilua::main
