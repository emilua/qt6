// Copyright (c) 2025 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#pragma once

#include <QtQuick/QQuickPaintedItem>
#include <QtGui/QImage>

namespace emilua_qt {

extern char qimage_mt_key;

class Image : public QQuickPaintedItem
{
    Q_OBJECT

    Q_PROPERTY(QVariant image READ image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(QSize sourceSize READ sourceSize NOTIFY imageChanged)

public:
    Image(QQuickItem* parent = nullptr);

    QVariant image() const;
    bool setImage(QVariant image);

    QSize sourceSize() const;

signals:
    void imageChanged();

protected:
    void paint(QPainter* painter) override;

private:
    QImage image_;
};

} // namespace emilua_qt
