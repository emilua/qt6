// Copyright (c) 2024 Vinícius dos Santos Oliveira
// SPDX-License-Identifier: MIT OR BSL-1.0

#pragma once

#include <QtGui/QStandardItemModel>
#include <emilua_qt/workqueue.hpp>

class QCoreApplication;
typedef struct lua_State lua_State;

namespace emilua_qt {

class StandardItemModel
{
public:
    struct TransparentStringHash
    {
        using is_transparent = void;

        std::size_t operator()(const std::string& s) const
        {
            return std::hash<std::string_view>{}(
                static_cast<std::string_view>(s));
        }

        std::size_t operator()(std::string_view s) const
        {
            return std::hash<std::string_view>{}(s);
        }
    };

    struct qobject_deleter
    {
        void operator()(QObject *o)
        {
            o->deleteLater();
        }
    };

    struct GUIData;

    StandardItemModel(int rows, int columns);
    StandardItemModel(StandardItemModel&& o) = default;
    StandardItemModel(const StandardItemModel&) = delete;

    void clear(WorkQueue* workQueue);
    void appendItem(WorkQueue* workQueue, std::unique_ptr<QStandardItem> item);
    void insertItem(WorkQueue* workQueue, int row,
                    std::unique_ptr<QStandardItem> item);
    void insertRows(WorkQueue* workQueue, int row, int count);
    void removeRows(WorkQueue* workQueue, int row, int count);
    void insertColumns(WorkQueue* workQueue, int column, int count);
    void removeColumns(WorkQueue* workQueue, int column, int count);
    void setItem(WorkQueue* workQueue, int row, int column,
                 std::unique_ptr<QStandardItem> item);
    void setRowCount(WorkQueue* workQueue, int rowCount);
    void setColumnCount(WorkQueue* workQueue, int columnCount);

    QStandardItem* item(int row, int column);

    static QVariant convertTopToVariant(lua_State* L);

    bool roleNamesAreSet = false;

    // when roleNamesAreSet=false, it contains just the reserved names:
    // https://doc.qt.io/qt-6/qabstractitemmodel.html#roleNames {{{
    QHash<int, QByteArray> roleNames;
    std::unordered_map<
        std::string, int,
        TransparentStringHash,
        std::equal_to<>
    > roleIds;
    // }}}

    std::unique_ptr<GUIData, qobject_deleter> guiData;
    std::vector<std::vector<std::unique_ptr<QStandardItem>>> rows;
    int rowCount;
    int columnCount;
};

struct StandardItemModel::GUIData
    : public QStandardItemModel //< data accessed from the GUI thread
{
    struct InsertItemUpdate { int row; std::unique_ptr<QStandardItem> item; };
    struct InsertRowsUpdate { int row; int count; };
    struct RemoveRowsUpdate { int row; int count; };
    struct InsertColumnsUpdate { int column; int count; };
    struct RemoveColumnsUpdate { int column; int count; };
    struct SetItemUpdate {
        int row; int column; std::unique_ptr<QStandardItem> item; };
    struct ClearUpdate {};
    using Update = std::variant<
        ClearUpdate, InsertItemUpdate, InsertRowsUpdate, RemoveRowsUpdate,
        InsertColumnsUpdate, RemoveColumnsUpdate, SetItemUpdate>;

    GUIData(int rows, int columns, QObject *parent = nullptr);

    void consumeUpdates();
    void update(std::lock_guard<std::mutex>&, WorkQueue* workQueue);

    // data accessed from both threads {{{
    std::vector<Update> updates;
    std::mutex updatesMtx;
    bool syncScheduled = false;
    // }}}
};

} // namespace emilua_qt
