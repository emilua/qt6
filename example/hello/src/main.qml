import QtCore
import QtQuick

Window {
    id: window
    width: 320; height: 480

    Settings {
        id: settings

        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
    }

    Rectangle {
        id: page
        anchors.fill: parent
        color: "lightgray"

        Text {
            text: "Hello World!"
            y: 30
            anchors.horizontalCenter: page.horizontalCenter
            font.pointSize: 24; font.bold: true
        }
    }
}
